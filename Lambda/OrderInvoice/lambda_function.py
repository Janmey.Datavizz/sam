# This function is created to get analytics insight for Orders in the last 24 hours.

# Import external and built-in libraries
import pymysql
import datetime
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()
# Using logging module
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In OrderAnalytics")
    try:
        with conn.cursor() as cur:
            OrderId = event["queryStringParameters"]['OrderId']
            cur.execute('SELECT CustomerMaster.`Cognito_Id`,CustomerMaster.`Customer_Id`,Restaurant_AddressMaster.`Branch_Location`,Restaurant_AddressMaster.`Branch_Name`,Restaurant_AddressMaster.`Contact_Number`,Restaurant_AddressMaster.`Email`,OrderItemList.`Item_Id`,OrderItemList.`Quantity`,OrderItemList.`OrderItemList_Id`,`OrderItemList`.`Comments`,`OrderItemList`.`Stuffing_Id`,`Order_StatusMaster`.`StatusName`,PaymentMaster.`Type`,Order_TypeMaster.`TypeName`,Restaurant_AddressMaster.FullName FROM OrderMaster INNER JOIN CustomerMaster ON OrderMaster.`Customer_Id`=CustomerMaster.`Customer_Id` INNER JOIN Restaurant_AddressMaster ON OrderMaster.`Branch_Id`=Restaurant_AddressMaster.`Restaurant_AddressId` INNER JOIN OrderItemList ON OrderMaster.`Order_Id`=OrderItemList.`Order_Id` INNER JOIN PaymentMaster ON OrderMaster.`Payment_Id` = PaymentMaster.`Payment_Id`INNER JOIN Order_StatusMaster ON OrderMaster.`Status_Id`= Order_StatusMaster.`Order_StatusId` INNER JOIN Order_TypeMaster ON OrderMaster.`Type_Id`=`Order_TypeMaster`.`Order_TypeId` WHERE OrderMaster.Order_Id = 14')
            tup = cur.fetchall()
            cur.execute('SELECT Tax_Slab, Fullname from RestaurantMaster where Name = "{}"'.format(tup[0][14]))
            data = cur.fetchall()
            logger.info(data)
            print(tup)
            holder = {}
            order_details = {}
            order_details['CognitoId'] = tup[0][0]
            order_details['CustomerId'] = tup[0][1]
            order_details['Branch_Location'] = tup[0][2]
            order_details['Branch_Name'] = tup[0][3]
            order_details['Restaurant_ContactNumber'] = tup[0][4]
            order_details['Restaurant_Email'] = tup[0][5]
            order_details['OrderStatus_Name'] = tup[0][11]
            order_details['TypeOfPayment'] = tup[0][12]
            order_details['TypeOfOrder'] = tup[0][13]
            order_details['Tax_Slab'] = data[0][0]
            order_details['Restaurant_Name'] = data[0][1]
            
            for row in range(0,len(tup)):
                cur.execute('select Name,Cost from FoodMaster where Item_Id = {}'.format(tup[row][6]))
                name = cur.fetchall()
                logger.info(tup[row][10])
                if tup[row][10] is None:
                    Stuffing_Name = "-"
                else:
                    cur.execute('select Name from StuffingMaster where Stuffing_Id = {}'.format(tup[row][10]))
                    stuff = cur.fetchall()
                    Stuffing_Name = stuff[0][0]
                order_items = {}
                order_items['Item_Id'] = tup[row][6]
                order_items['Item_Name'] = name[0][0]
                order_items['Cost'] = name[0][1]
                order_items['Quantity'] = tup[row][7]
                order_items['OrderItem_ListId'] = tup[row][8]
                order_items['Comments'] = tup[row][9]
                order_items['Stuffing_Name'] = Stuffing_Name
                order_details.update({row:order_items})
                
            holder.update({"Items":order_details})
            cur.close()
            print(holder)
            holder1 = json.dumps(holder,default =str)
            print(holder1)
            content = json.loads(holder1)
            out = {}
            if len(content) > 0:
                out["statusCode"] = 200
                header={}
                header['Access-Control-Allow-Origin']="*"
                out["headers"]=header
                out["body"] = content
            else:
                out["statusCode"] = 201
                header={}
                header['Access-Control-Allow-Origin']="*"
                out["headers"]=header
                out["body"] = "NO DATA"
        return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps(out)}

    except Exception as e:
        logger.info(e)
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps(e)}
