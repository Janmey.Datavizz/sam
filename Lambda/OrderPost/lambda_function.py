# This function is created for activating/deactivating orders also noting deactivated time.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from pytz import timezone
import datetime
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)
# Using logging module.
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
def handler(event, context):
    logger.info("In Orderpost handler")
    orderstatus = json.loads(event['body'])
    logger.debug("Event Body: {}".format(orderstatus))
    try:
        with conn.cursor() as cur:
            cur.execute('Select Order_StatusId from Order_StatusMaster where StatusName="{}"'.format(orderstatus['StatusName']))
            tup = cur.fetchall()
            logger.debug("Query Output: {}".format(tup))
            # print(tup[0])
            # print(tup[0][0])
            # print(date)
            if tup[0][0] == 2:
                cur.execute('Update OrderMaster set Status_Id = "2",AcceptedAt="{}" where Order_Id="{}"'.format(date,orderstatus['Order_Id']))
                conn.commit()
            elif tup[0][0] == 3:
                cur.execute('Update OrderMaster set Status_Id = "3" where Order_Id="{}"'.format(orderstatus['Order_Id']))
                conn.commit()
            elif tup[0][0] == 4:
                cur.execute('Update OrderMaster set Status_Id = "4",DeliveredAt="{}" where Order_Id="{}"'.format(date,orderstatus['Order_Id']))
                conn.commit()
            elif tup[0][0] == 5:
                cur.execute('Update OrderMaster set Status_Id = "5" where Order_Id="{}"'.format(orderstatus['Order_Id']))
                conn.commit()
            # Close conn.cursor()
            cur.close()
        return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
    except:
        logger.error("ERROR: There is a problem in SQL query")
        return {"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error")}
        sys.exit()