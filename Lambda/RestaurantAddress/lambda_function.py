# This function is created to get data from item_list Table

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()

# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In RestaurantAddress Handler")
    try:
        with conn.cursor() as cur:
            cur.execute("Select * from Restaurant_AddressMaster where Status = 1")
            tup = cur.fetchall()
            logger.debug("Query Output: {}".format(tup))
            conn.commit()
            holder = {}
            for row in range(0,len(tup)):
                item_details = {}
                item_details['Restaurant_AddressId'] = tup[row][0]
                item_details['Restaurant_Id'] = tup[row][1]
                item_details['Fullname'] = tup[row][2]
                item_details['Email'] =tup[row][3]
                item_details['Password'] =tup[row][4]
                item_details['Contact_Number'] = tup[row][5]
                item_details['Branch_Name'] = tup[row][6]
                item_details['Branch_Location'] = tup[row][7]
                item_details['Laitude'] = tup[row][8]
                item_details['Longitude'] = tup[row][9]
                item_details['Status'] = tup[row][10]
                item_details['EDT'] = tup[row][11]
                item_details['Delivery_Radius'] = tup[row][12]
                item_details['Delivery_Charge'] = tup[row][13]
                item_details['Delivery_Option'] = tup[row][14]
                item_details['Payment_Option'] = tup[row][15]
                item_details['CreatedAt'] = tup[row][16]
                item_details['UpdatedAt'] = tup[row][17]
                holder.update({row:item_details})
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            
            # Close conn.cursor()
            cur.close()
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
        logger.info("Output JSON response: {}".format(out))
        return out
    except:
        logger.error("ERROR: There is a problem in SQL query")
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data")}
        sys.exit()

