# This function is created for inserting item data to ItemDuplicate and Item Table.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from pytz import timezone
import datetime
from thundra.thundra_agent import Thundra
thundra = Thundra()

fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)

# Using logging module
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In Orderput handler")
    order = json.loads(event["body"])
    logger.debug("Event body: {}".format(order))
    try:
        with conn.cursor() as cur:
            cur.execute("select Restaurant_AddressId from Restaurant_AddressMaster where Branch_Name='{}'".format(branch))
            branchtup = cur.fetchall()
            logger.info(branchtup[0][0]) 
            
            cur.execute("select Order_TypeId from Order_TypeMaster where TypeName='{}'".format(ordertype))
            ordertypetup = cur.fetchall()
            logger.info(ordertypetup[0][0])
            
            cur.execute("select Payment_Id from PaymentMaster where Type='{}'".format(payment_value))
            paymenttup = cur.fetchall()
            
            
            
            cur.close()    
            return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
            
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query")
        logger.error(e)
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error in items")}
        sys.exit()
    

