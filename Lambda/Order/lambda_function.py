# This function is created to fetch orders from order_list table

# Import external and built-in module
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()
# Using logging module
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In Order handler")
    try:
        with conn.cursor() as cur:
            if "queryStringParameters" in event:
                if 'orderstatus' in event["queryStringParameters"]:
                    logger.info("Orderstatus in event")
                    order_status = event["queryStringParameters"]['orderstatus']
                if 'ordertype' in event["queryStringParameters"]:
                    logger.info("Ordertype in event")
                    order_type = event["queryStringParameters"]['ordertype']
                if 'orderid' in event["queryStringParameters"]:
                    logger.info("Orderid in event ")
                    order_id = event["queryStringParameters"]['orderid']
            try:
                finalQuery = "SELECT OM.Order_Id,OM.Branch_Id,RAM.Branch_Name,OM.`Status_Id`,OSM.StatusName,OM.Payment_Id,OTM.`Order_TypeId`,OTM.TypeName,OM.CreatedAt,OM.AcceptedAt,OM.DeliveredAt,OM.Coupon_Id,CM.`Cognito_Id`,CM.`Customer_Id`,PM.`Type` FROM OrderMaster AS OM INNER JOIN Restaurant_AddressMaster AS RAM ON OM.Branch_Id = RAM.Restaurant_AddressId INNER JOIN Order_StatusMaster AS OSM ON OM.Status_Id=OSM.Order_StatusId INNER JOIN Order_TypeMaster AS OTM ON OM.Type_Id=OTM.Order_TypeId INNER JOIN PaymentMaster AS PM ON OM.`Payment_Id`=PM.`Payment_Id` INNER JOIN CustomerMaster AS CM ON OM.`Customer_Id`=CM.`Customer_Id`"
            except Exception as e:
                logger.error(e)
            if len(order_status) != 0:
                finalQuery+=" and OSM.Order_StatusId='" + order_status + "' "
            if len(order_type) != 0:
                finalQuery+=" and OTM.Order_TypeId='" + order_type + "' "
            if len(order_id) != 0:
                finalQuery+=" and OM.Order_Id='" + order_id + "' "
            cur.execute(finalQuery)
            tup = cur.fetchall()
        
            logger.debug("Query Output1: {}".format(tup))
            holder = {}
            for row in range(0,len(tup)):
                try:
                    cur.execute("select count(Item_Id) from OrderItemList where Order_Id = {}".format(tup[row][0]))
                    item = cur.fetchall()
                except Exception as e:
                    logger.info(e)
                order_details = {}
                order_details['Order_Id'] = tup[row][0]
                order_details['Branch_Id'] = tup[row][1]
                order_details['Branch_Name'] = tup[row][2]
                order_details['Status_Id'] = tup[row][3]
                order_details['Status_Name'] =tup[row][4]
                order_details['Payment'] =tup[row][5]
                order_details['Type_Id'] = tup[row][6]
                order_details['TypeName'] =tup[row][7]
                order_details['CreatedAt'] = tup[row][8]
                order_details['AcceptedAt'] = tup[row][9]
                order_details['DeliveredAt'] =tup[row][10]
                order_details['Coupon_Id'] = tup[row][11]
                order_details['TotalItem']= item[0][0]
                order_details['Cognito_Id']=tup[row][12]
                order_details['Customer_Id']=tup[row][13]
                order_details['Order_PaymentType']=tup[row][14]
                holder.update({tup[row][0]:order_details}) 
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
            cur.close()
        # conn.close()    
            return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps(out)}
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query {}".format(e))
        logger.error(e)
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data")}
        sys.exit()

