# This function is created for activating/deactivating items.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()

# Using logging module
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)
from pytz import timezone
import datetime
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)
# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In PaymentDetailMasterPost Handler")
    item = json.loads(event['body'])
    logger.debug("Event Body: {}".format(item)) 
    
    # json object should contain the name and restaurant branch id
    try:
        with conn.cursor() as cur:
            
            # here the restaurant id should be the is of particular branch
            cur.execute("Select Payment_Id from PaymentMaster where Type='{}'".format(item['Type']))
            tup = cur.fetchall()
            Id = [i[0] for i in tup]
            logger.debug("Query Output: {}".format(tup))
            if len(Id) != 0:
                
                for i in Id:
                    cur.execute("Select Status from Restaurant_AddressMaster where Branch_Name = ('{}') and Restaurant_Id = ('{}')".format(item['Branch_Name'],item['Restaurant_Id']))
                    tup = cur.fetchall()
                    logger.debug("status: {}".format(tup))
                    
                    if (1,) == tup[0]:    
                        cur.execute("Update PaymentDetailMaster set Status = 0,UpdatedAt='{}' where PaymentType_Id= ('{}') and Restaurant_Id = ('{}')".format(date,i,item['Restaurant_Id']))
                        
                    elif (0,) == tup[0]:
                        cur.execute("Update PaymentDetailMaster set Status = 1,UpdatedAt='{}' where PaymentType_Id= ('{}') and Restaurant_Id = ('{}')".format(date,i,item['Restaurant_Id']))
                    
                
                cur.close()
                return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
           
            
            else:
                
                return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Type not added")}
    
    except:
        logger.error("ERROR: There is a problem in SQL ERROR")
        return{"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error")}
        sys.exit()
        
        
        
