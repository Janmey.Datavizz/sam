# This function is created to get data from special_item_list Table

# Import external and built-in libraries 
import pymysql
import sys
import logging
import os
import json
import datetime

# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']
from thundra.thundra_agent import Thundra
thundra = Thundra()


from pytz import timezone
import datetime
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In Itemput handler")
    time = json.loads(event["body"])
    timest = datetime.datetime.now()
    logger.debug("Event body: {}".format(time))
    try:
        with conn.cursor() as cur:
            
            cur.execute('select * from TimeMaster')
            logger.info(cur.rowcount)
            if not cur.rowcount:
                cur.execute('Insert into TimeMaster(Restaurant_Id,Day,Pickup_OpenTime,Pickup_CloseTime,Delivery_OpenTime,Delivery_CloseTime,Status,CreatedAt,UpdatedAt) values({},{},"{}","{}","{}","{}",1,"{}","{}")'.format(time['Restaurant_Id'],time['Day'],time['Pickup_OpenTime'],time['Pickup_CloseTime'],time['Delivery_OpenTime'],time['Delivery_CloseTime'],date))
                conn.commit()
                logger.info("Insert into TimeMaster Initially query executed")
                return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
            else:
                cur.execute('select max(Time_Id) from TimeMaster')
                tup4 = cur.fetchall()
                logger.info(tup4[0][0])
                cur.execute('alter table TimeMaster AUTO_INCREMENT={}'.format(tup4[0][0]))
                conn.commit()
                cur.execute('Insert into TimeMaster(Restaurant_Id,Day,Pickup_OpenTime,Pickup_CloseTime,Delivery_OpenTime,Delivery_CloseTime,Status,CreatedAt,UpdatedAt) values({},{},"{}","{}","{}","{}",1,"{}","{}")'.format(time['Restaurant_Id'],time['Day'],time['Pickup_OpenTime'],time['Pickup_CloseTime'],time['Delivery_OpenTime'],time['Delivery_CloseTime'],date))
                conn.commit()
                logger.info("Insert into TimeMaster Initially query executed")
                return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}

    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query")
        logger.error(e)
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error in items")}
        sys.exit()
    


