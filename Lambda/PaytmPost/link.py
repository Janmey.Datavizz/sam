import requests 
import json
import Checksum
import sys
import logging
import os
logger = logging.getLogger()
logger.setLevel(logging.INFO)
REGION = 'us-east-1'
from main import db

main1 = db()
main2 = db()
main3 = db()
#MERCHANT_MID = "BAgrPt43006748913201"
#MERCHANT_KEY = "t_e%fbQS4E%B#Ix&"

def handler(event, context):
	
	content = json.loads(event['body'])
	logger.info("content: {}".format(content))	
	Payment_Id = main2.handle2(content['RestaurantAddress_Id'])
	logger.info("paymentid : {}".format(Payment_Id))
	logger.info(Payment_Id[0])
	Keys = main3.handle3(Payment_Id[0])
	logger.info("keys : {}".format(Keys))
	#Cdata = main1.handle1(content['Customer_Id'])
	#logger.info(Cdata)
	
	paytmParams = {}	
	paytmParams["body"] = {
	"mid" : Keys[0][3],
	"linkName" : "ProTest",
	"linkDescription" : "Payment link",
	"linkType" : "FIXED",
	"amount" : content['amount'],
	"expiryDate" : "10/04/2020",
	"isActive" : "true",
	"sendSms" : "true",
	"sendEmail" : "true",
	"customerContact" : {
	"customerName" : content['Name'],
	"customerEmail" : content['Email'],
	"customerMobile" : content['Mobile'],
	"CALLBACK_URL" : "https://8yqsujfjhh.execute-api.us-east-1.amazonaws.com/dev/"
	}
	}
	checksum = Checksum.generate_checksum_by_str(json.dumps(paytmParams["body"]), Keys[0][2])
	paytmParams["head"] = {"timestamp" : "1554708506","clientId" : "C22","version" : "v1","channelId" : "WEB","tokenType" : "AES","signature" : checksum}

	post_data = json.dumps(paytmParams)
	print (post_data)

	# for staging
	url = "https://securegw-stage.paytm.in/link/create"
	# for production
	# url = "https://securegw.paytm.in/link/create"
	response = requests.post(url = url, data = post_data, headers = {"Content-type" : "application/json"})
	print (response)
	return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps(paytmParams)}
	
	