import Checksum
import requests
import json

MERCHANT_MID = "BAgrPt43006748913201"
MERCHANT_KEY = "t_e%fbQS4E%B#Ix&"

paytmParams = dict()
paytmParams["body"] = {
"mid" : MERCHANT_MID,
"txnType" : "REFUND",
"orderId" : "order0001",
"refId" : "ref150",
"txnId" : "12",
"refundAmount" : "100.00"
}

checksum = Checksum.generate_checksum_by_str(json.dumps(paytmParams["body"]), MERCHANT_KEY)

paytmParams["head"] = {
"clientId" : "C11",
"version" : "v1",
"signature" : checksum
}

post_data = json.dumps(paytmParams)

url = "https://securegw-stage.paytm.in/refund/api/v1/async/refund" # for staging
# url = "https://securegw.paytm.in/refund/api/v1/async/refund" # for production

response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
print (response)
