# This function is created to get data from item_list Table

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()


# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In OrderType Handler")
    try:
        with conn.cursor() as cur:
            cur.execute("Select Order_TypeId,TypeName,TypeStatus,CreatedAt,UpdatedAt from Order_TypeMaster where TypeStatus = 1")
            tup = cur.fetchall()
            logger.debug("Query Output: {}".format(tup))
            conn.commit()
            holder = {}
            for row in range(0,len(tup)):
                item_details = {}
                #item_details['Order_TypeId'] = tup[row][0]
                item_details['TypeName'] = tup[row][1]
                item_details['TypeStatus'] = tup[row][2]
                item_details['CreatedAt'] =tup[row][3]
                item_details['UpdatedAt'] =tup[row][4]
                holder.update({tup[row][0]:item_details})
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            
            # Close conn.cursor()
            cur.close()
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
        logger.info("Output JSON response: {}".format(out))
        return out
    except:
        logger.error("ERROR: There is a problem in SQL query")
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data")}
        sys.exit()

