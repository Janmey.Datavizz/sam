# This function is created to fetch data from item_category_master Table.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
#from thundra.thundra_agent import Thundra
#thundra = Thundra()
#Using logging library
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = "twiliodb.cmsec242mlpl.us-east-1.rds.amazonaws.com"
name = "admin"
password = "admin123"
db_name = "KBOBS"

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
#@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In Category Handler")
    try:
        with conn.cursor() as curr:
            curr.execute("Select Category_Id,Name,CreatedAt,UpdatedAt from CategoryMaster where Status = 1 ")
            tup = curr.fetchall()
            logger.debug("Query Output: {}".format(tup))
            holder = {}
            for row in range(0,len(tup)):
                category={}
                category['Category_Id'] = tup[row][0]
                category['Name'] = tup[row][1]
                category['CreatedAt'] = tup[row][2]
                category['UpdatedAt'] = tup[row][3]
                holder.update({row:category})
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            
            # Close conn.cursor()
            curr.close()
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
        logger.debug("Output JSON response: {}".format(out))
        return out
    except Exception as e:
        print(e)
        logger.error("ERROR: There is a problem in SQL query")
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data")}
        sys.exit()