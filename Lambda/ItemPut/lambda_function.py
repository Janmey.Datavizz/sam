# This function is created for inserting item data to ItemDuplicate and Item Table.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from pytz import timezone
import datetime
from thundra.thundra_agent import Thundra
thundra = Thundra()
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)

# Using logging module
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In Itemput handler")
    item = json.loads(event["body"])
    logger.debug("Event body: {}".format(item))
    
    try:
        with conn.cursor() as cur:
            try:
                cur.execute("select max(Item_Number) from FoodMaster ")
                tup = cur.fetchall()
                number = [i[0] for i in tup]
                Item_Number = number[0] + 1
                logger.info("number: {}".format(number))
            except Exception as e:
                logger.info(e)

            if 'stuffing' not in item:
                logger.info("not in stuffing")
                cur.execute('Insert into FoodMaster(Name,Item_Number,Category_Id,Type_Id,Cost,Item_Status,ChefSpecial_Status,MenuSpecial_Status,Description,CreateDate,LastUpdate) values("{}",{},{},{},{},1,{},{},"{}","{}","{}")'.format(item['Name'],Item_Number,item['Category_Id'],item['Type_Id'],item['Cost'],item['ChefSpecial_Status'],item['MenuSpecial_Status'],item['Description'],date,date))
                conn.commit()                
                logger.info("Insert into Food_Master query executed")
                return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("New Item Added name {}".format(item['Name']))}  
            
            else:
                for i in item['stuffing']:
                    cur.execute('Insert into FoodMaster(Name,Item_Number,Category_Id,Type_Id,Stuffing_Id,Cost,Item_Status,ChefSpecial_Status,MenuSpecial_Status,Description,CreateDate,LastUpdate) values("{}",{},{},{},{},{},1,{},{},"{}","{}","{}")'.format(item['Name'],Item_Number,item['Category_Id'],item['Type_Id'],item['stuffing'][i]['stuffing_id'],item['stuffing'][i]['cost'],item['ChefSpecial_Status'],item['MenuSpecial_Status'],item['Description'],date,date))
                    conn.commit()
                    logger.info("Insert into Food_Master query executed")
                return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("New Item Added name {}".format(item['Name']))}
            cur.close()
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query")
        logger.error(e)
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error in items {}".format(e))}
        sys.exit()