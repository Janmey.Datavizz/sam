# This function is created for activating/deactivating itemcategory.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from pytz import timezone
import datetime
from thundra.thundra_agent import Thundra
thundra = Thundra()

fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)
# Using logging module
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance   
    """
    
    logger.info("In Add restaurant")
    data = json.loads(event['body'])
    logger.debug("Event Body: {}".format(data))
    try:
        with conn.cursor() as curr:
            
            curr.execute("Insert into RestaurantMaster(Name,Fullname,Email,Password,Number,GST_Id,Business_Name,Tax_Slab,Status,CreatedAt,UpdatedAt) values('{}','{}','{}','{}','{}','{}','{}','{}',1,'{}','{}') ".
                format(data['Name'],data['Fullname'],data['Email'],data['Password'],data['Number'],data['GST_Id'],data['Business_Name'],data['Tax_Slab'],date,date))      
        logger.info("Insert Query executed")
        conn.commit()
        conn.close()
        return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
    except Exception as e:
        logger.info(e)
        logger.error("ERROR: There is a problem in SQL query")
        return {"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error")}
        sys.exit()
