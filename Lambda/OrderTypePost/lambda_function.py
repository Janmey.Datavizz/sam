# This function is created to get data from item_list Table

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from pytz import timezone
import datetime
from thundra.thundra_agent import Thundra
thundra = Thundra()

fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)

# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In OrderTypePost Handler")
    ordertype = json.loads(event['body'])
    try:
        with conn.cursor() as cur:
            cur.execute('Select TypeStatus from Order_TypeMaster where Order_TypeId="{}"'.format(ordertype['ordertypeid']))
            tup = cur.fetchall()
            logger.debug("Query Output: {}".format(tup))
            if (1,) == tup[0]:
                cur.execute('Update Order_TypeMaster set TypeStatus = "0",UpdatedAt="{}" where Order_TypeId="{}"'.format(date,ordertype['ordertypeid']))
                conn.commit()
            elif (0,) == tup[0]:
                cur.execute('Update Order_TypeMaster set TypeStatus = "1",UpdatedAt="{}" where Order_TypeId="{}"'.format(date,ordertype['ordertypeid']))
                conn.commit()
                
                # Close conn.cursor()
            cur.close()
        return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
    except:
        logger.error("ERROR: There is a problem in SQL ERROR")
        return{"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error")}
        sys.exit()

