# This function is created to get data from special_item_list Table

# Import external and built-in libraries 
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()


# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In Special Handler")
    try:
        with conn.cursor() as cur:
            cur.execute("SELECT Special_ItemId,SpecialMaster.Name,CategoryMaster.Name,TypeMaster.`Name`,Stuffing_Id,SpecialMaster.Cost,SpecialMaster.Status,SpecialDays,Description,CreateDate,EndDate FROM SpecialMaster JOIN CategoryMaster ON SpecialMaster.`Category_Id`=CategoryMaster.`Category_Id` JOIN TypeMaster ON SpecialMaster.`Type_Id`=TypeMaster.`Type_Id`  WHERE SpecialMaster.`Status`=1")
            tup = cur.fetchall()
            logger.debug("Query Tuple: {}".format(tup))
            conn.commit()
            holder = {}
            for row in range(0,len(tup)):
                if tup[row][4] is None:
                    special_details = {}
                    special_details['Special_ItemId'] = tup[row][0]
                    special_details['Name'] = tup[row][1]
                    special_details['Category'] = tup[row][2]
                    special_details['Type'] =tup[row][3]
                    special_details['Stuffing'] =tup[row][4]
                    special_details['Cost'] = tup[row][5]
                    special_details['Status'] = tup[row][6]
                    special_details['SpecialDays'] = tup[row][7]
                    special_details['Description'] = tup[row][8]
                    special_details['CreateDate'] = tup[row][9]
                    special_details['EndDate'] = tup[row][10]
                elif tup[row][4] is not None:
                    cur.execute("select Name from StuffingMaster where Stuffing_Id = {}".format(tup[row][4]))
                    id = cur.fetchall()
                    stuffid = [i[0] for i in id]
                    special_details = {}
                    special_details['Special_ItemId'] = tup[row][0]
                    special_details['Name'] = tup[row][1]
                    special_details['Category'] = tup[row][2]
                    special_details['Type'] =tup[row][3]
                    special_details['Stuffing'] = stuffid[0]
                    special_details['Cost'] = tup[row][5]
                    special_details['Status'] = tup[row][6]
                    special_details['SpecialDays'] = tup[row][7]
                    special_details['Description'] = tup[row][8]
                    special_details['CreateDate'] = tup[row][9]
                    special_details['EndDate'] = tup[row][10]
                    
                holder.update({row:special_details})
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            
            # Close conn.cursor()
            cur.close()
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
        logger.debug("Output JSON response: {}".format(out))
        return out
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query")
        logger.error(e)
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data")}
        sys.exit()
