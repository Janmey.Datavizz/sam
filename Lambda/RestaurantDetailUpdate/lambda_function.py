# This function is created for activating/deactivating items.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json

# Using logging module
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
def handler(event, context):   
    logger.info("In restaurant Handler")
    item = json.loads(event['body'])
    logger.debug("Event Body: {}".format(item))
    
    try:
        with conn.cursor() as cur:
            cur.execute('Select Status from RestaurantMaster where Restaurant_Id="{}"'.format(item['Restaurant_Id']))
            tup = cur.fetchall() 
            logger.debug("Query Output: {}".format(tup))
            if (1,) == tup[0]:
                cur.execute("Update RestaurantMaster set Name= ('{}'), Fullname = ('{}'),Email= ('{}'),Password = ('{}'),Number = ('{}'),GST_Id=('{}'),Business_Name = ('{}'),Tax_Slab= ('{}'),UpdatedAt=CURRENT_TIMESTAMP where Restaurant_Id='{}'".format(item['Name'],item['Fullname'],item['Email'],item['Password'],item['Number'],item['GST_Id'],item['Business_Name'],item['Tax_Slab'],item['Restaurant_Id']))
                conn.commit()
                # Close conn.cursor()
                cur.close()
                return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
    
            elif (0,) == tup[0]:
                
                return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Restraunt is not active")}
    
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL ERROR {}".format(e))
        return{"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error {}".format(e)}
        sys.exit()

