# This function is created to get data from special_item_list Table

# Import external and built-in libraries 
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()
# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In Customer Handler")
    try:
        with conn.cursor() as cur:
            restaurantaddress_id = event["queryStringParameters"]["restaurantaddress_id"]
            cur.execute("Select * from TimeMaster where Restaurant_id = {}".format(restaurantaddress_id))
            tup = cur.fetchall()
            cur.execute("select FullName ,Branch_Name  from Restaurant_AddressMaster where Restaurant_AddressId = {}".format(restaurantaddress_id))
            name = cur.fetchall()
            logger.debug("Query Tuple: {}".format(tup))
            conn.commit()
            holder = {}
            for row in range(0,len(tup)):
                special_details = {}
                special_details['Time_Id'] = tup[row][0]
                special_details['Restaurant_Name'] = name[0][0]
                special_details['Branch_Name'] = name[0][1]
                special_details['Day'] = tup[row][2]
                special_details['Pickup_OpenTime'] = tup[row][3]
                special_details['Pickup_CloseTime'] = tup[row][4]
                special_details['Delivery_OpenTime'] = tup[row][5]
                special_details['Delivery_CloseTime'] = tup[row][6]
                special_details['Status'] = tup[row][7]
                special_details['CreatedAt'] = tup[row][8]
                special_details['UpdatesAt'] = tup[row][9]
                holder.update({row:special_details})
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            
            # Close conn.cursor()
            cur.close()
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
        logger.debug("Output JSON response: {}".format(out))
        return out
    except Exception as e:
        print(e)
        logger.error("ERROR: There is a problem in SQL query")
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data")}
        sys.exit()