# This function is created for activating/deactivating items.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()

# Using logging module
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In Itempost Handler")
    item = json.loads(event['body'])
    logger.debug("Event Body: {}".format(item)) 
    
    
    try:
        with conn.cursor() as cur:
            
            # here the restaurant id should be the is of particular branch
            cur.execute('Select PaymentType_Id from PaymentDetailMaster where Restaurant_Id="{}"'.format(item['Restaurant_Id']))
            tup = cur.fetchall()
            logger.debug("Query Output: {}".format(tup))
            if (1,) == tup[0]:
                cur.execute("select PaymentDetail_Id from PaymentDetailMaster where Restaurant_Id= ('{}') and PaymentType_Id = 1".format(item['Restaurant_Id'],))
                tup = cur.fetchall()
                Id = [i[0] for i in id]
                cur.execute("Update PaytmMaster set Merchant_Key = ('{}'),Merchant_Mid= ('{}'),Mobile_Number= ('{}'),UpdatedAt=CURRENT_TIMESTAMP where Payment_Id='{}'".format(item['Merchant_Key'],item['Merchant_Mid'],item['Mobile_Number'],id))
                
                # Close conn.cursor()
                cur.close()
            elif (0,) == tup[0]:
                cur.execute('Update RestaurantMaster set status = "1",UpdatedAt=CURRENT_TIMESTAMP where Restaurant_Id="{}"'.format(item['Restaurant_Id']))
                conn.commit()
                
                # Close conn.cursor()
                cur.close()
        return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
    except:
        logger.error("ERROR: There is a problem in SQL ERROR")
        return{"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error")}
        sys.exit()
