# This function is created to get analytics insight for Orders in the last 24 hours.

# Import external and built-in libraries
import pymysql
import datetime
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()
# Using logging module
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In OrderAnalytics")
    try:
        with conn.cursor() as cur:
            StartDate = event["queryStringParameters"]['StartDate']
            EndDate = event["queryStringParameters"]['EndDate']
            logger.info("Query string parameters: {},{}".format(StartDate,EndDate))
            cur.execute('SELECT COUNT(a.Order_Id) AS OrderCount,b.StatusName FROM OrderMaster a, Order_StatusMaster b WHERE a.`Status_Id`=b.`Order_StatusId` AND a.CreatedAt BETWEEN "{}" AND "{}" GROUP BY Status_Id '.format(StartDate,EndDate))
            tup = cur.fetchall()
            conn.commit()
            holder = {}
            
            for row in range(0,len(tup)):
                order_details = {}
                order_details['OrderCount'] = tup[row][0]
                order_details['StatusName'] = tup[row][1]
                holder.update({row:order_details})
            cur.close()
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            out = {}
            if len(content) > 0:
                out["statusCode"] = 200
                header={}
                header['Access-Control-Allow-Origin']="*"
                out["headers"]=header
                out["body"] = content
            else:
                out["statusCode"] = 201
                header={}
                header['Access-Control-Allow-Origin']="*"
                out["headers"]=header
                out["body"] = "NO DATA"
        return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps(out)}

    except Exception as e:
        logger.info(e)
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps(e)}