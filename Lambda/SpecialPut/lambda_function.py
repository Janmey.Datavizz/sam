# This function is created for inserting special item data to special_item_list Table.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from pytz import timezone
import datetime
from thundra.thundra_agent import Thundra
thundra = Thundra()

fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)
# Using logging module
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In Specialput Handler")
    special = json.loads(event['body'])
    logger.debug("Event Body: {}".format(special))
    try:
        with conn.cursor() as cur:
            cur.execute('select * from SpecialMaster')
            logger.info(cur.rowcount)
            if not cur.rowcount:
                a = special['specialDays'].split(",")
                for i in range(0,len(a)):
                    specialDays = a[i]
                    cur.execute("Insert into SpecialMaster(Name,Category_Id,Type_Id,Stuffing_Id,Description,Status,SpecialDays,CreateDate,EndDate) values('{}',{},{},{},'{}','1',{},'{}','{}')".format(special['Name'],special['Category_Id'],special['Type_Id'],special['Stuffing_Id'],special['Description'],specialDays,date,date))
                conn.commit()
                cur.close()
            else:
                cur.execute('select max(Special_ItemId) from SpecialMaster')
                tup4 = cur.fetchall()
                logger.info(tup4[0][0])
                cur.execute('alter table SpecialMaster AUTO_INCREMENT={}'.format(tup4[0][0]))
                conn.commit()
                a = special['specialDays'].split(",")
                for i in range(0,len(a)):
                    specialDays = a[i]
                    cur.execute("Insert into SpecialMaster(Name,Category_Id,Type_Id,Stuffing_Id,Description,Status,SpecialDays,CreateDate) values('{}',{},{},{},'{}','1',{},'{}')".format(special['Name'],special['Category_Id'],special['Type_Id'],special['Stuffing_Id'],special['Description'],specialDays,date))
                conn.commit()
                cur.close()
        return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
    except Exception as e:
        logger.error("ERROR: There is a problem in external SQL query")
        logger.error(e)
        return {"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error adding item")}
        sys.exit()

