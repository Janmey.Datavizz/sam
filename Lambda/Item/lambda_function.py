# This function is created to get data from item_list Table

# Import external and built-in libraries
import pymysql 
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra


# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = 'twiliodb.cmsec242mlpl.us-east-1.rds.amazonaws.com'
name = 'admin'
password = 'admin123'
db_name = 'KBOBS'

#thundra = Thundra()
# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

#@thundra
# Main Function
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In Item Handler")
    try:
        with conn.cursor() as cur:
            cur.execute("SELECT Item_Id,FoodMaster.Name,FoodMaster.Category_Id,CategoryMaster.Name,FoodMaster.Type_Id,TypeMaster.Name,FoodMaster.Stuffing_Id,FoodMaster.Cost,FoodMaster.Item_Status,FoodMaster.ChefSpecial_Status,FoodMaster.MenuSpecial_Status,FoodMaster.Description,FoodMaster.CreateDate,FoodMaster.LastUpdate FROM FoodMaster INNER JOIN CategoryMaster ON FoodMaster.`Category_Id`=CategoryMaster.`Category_Id` INNER JOIN TypeMaster ON FoodMaster.`Type_Id`=TypeMaster.`Type_Id` WHERE FoodMaster.`Item_Status`=1")
            tup = cur.fetchall()
            logger.debug("Query Output: {}".format(tup))
            conn.commit()
            holder = {}
            for row in range(0,len(tup)):
                if tup[row][6] is None:
                    item_details = {}
                    item_details['Item_Id'] = tup[row][0]
                    item_details['Name'] = tup[row][1]
                    item_details['Category_Id'] = tup[row][2]
                    item_details['Category'] = tup[row][3]
                    item_details['Type_Id'] = tup[row][4]
                    item_details['Type'] =tup[row][5]
                    item_details['Stuffing'] =tup[row][6]
                    item_details['Cost'] = tup[row][7]
                    item_details['Item_Status'] = tup[row][8]
                    item_details['ChefSpecial_Status'] = tup[row][9]
                    item_details['MenuSpecial_Status'] = tup[row][10]
                    item_details['Description'] = tup[row][11]
                    item_details['CreateDate'] = tup[row][12]
                    item_details['LastUpdate'] = tup[row][13]
                elif tup[row][6] is not None:
                    cur.execute("select Name from StuffingMaster where Stuffing_Id = {}".format(tup[row][4]))
                    id = cur.fetchall()
                    stuffid = [i[0] for i in id]
                    item_details = {}
                    item_details['Item_Id'] = tup[row][0]
                    item_details['Name'] = tup[row][1]
                    item_details['Category_Id'] = tup[row][2]
                    item_details['Category'] = tup[row][3]
                    item_details['Type_Id'] = tup[row][4]
                    item_details['Type'] =tup[row][5]
                    item_details['Stuffing'] =stuffid[0]
                    item_details['Cost'] = tup[row][7]
                    item_details['Item_Status'] = tup[row][8]
                    item_details['ChefSpecial_Status'] = tup[row][9]
                    item_details['MenuSpecial_Status'] = tup[row][10]
                    item_details['Description'] = tup[row][11]
                    item_details['CreateDate'] = tup[row][12]
                    item_details['LastUpdate'] = tup[row][13]
                    
                holder.update({row:item_details})
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            
            # Close conn.cursor()
            cur.close()
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
        logger.info("Output JSON response: {}".format(out))
        return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps(out)}
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query {}".format(e))
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data")}
        sys.exit()