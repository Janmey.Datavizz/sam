# This function is created for inserting new branch of restaurant

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
import time
from pytz import timezone
import datetime
from thundra.thundra_agent import Thundra
thundra = Thundra()
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)

# Using logging module
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance   
    """
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    logger.info("timestamp: {}".format(st))
    logger.info("In Add restaurant")
    data = json.loads(event['body'])
    logger.debug("Event Body: {}".format(data))
    try:
        with conn.cursor() as curr:
            
            curr.execute("select Payment_Id from PaymentMaster")
            id = curr.fetchall()
            id1  = [i[0] for i in id]
            logger.info("Id are :{}".format(id1))
            
            curr.execute("Insert into Restaurant_AddressMaster(Restaurant_Id,Fullname,Email,Password,Contact_Number,Branch_Name,Branch_Location,Latitude,Longitude,Status,EDT,Delivery_Radius,Delivery_Charge,Delivery_Option,Payment_Option,CreatedAt,UpdatedAt) values({},'{}','{}','{}','{}','{}','{}','{}','{}',1,'{}','{}','{}','{}','{}','{}','{}') ".
                format(data['Restaurant_Id'],data['Fullname'],data['Email'],data['Password'],data['Contact_Number'],data['Branch_Name'],data['Branch_Location'],data['Latitude'],data['Longitude'],data['EDT'],data['Delivery_Radius'],data['Delivery_Charge'],data['Delivery_Option'],data['Payment_Option'],date,date))      
            logger.info("Insert Query executed for Restaurant_AddressMaster")
            
            
            for i in range(1,8):
                logger.info("day is:'{}'".format(data['Timings'][Day[i]]['Day']))
            
            try:
                with conn.cursor() as curr:
                    for i in range(1,8):
                        DAY = Day[i]
                        curr.execute("Insert into TimeMaster (Restaurant_id,Day,Pickup_OpenTime,Pickup_CloseTime,Delivary_OpenTime,Delivary_CloseTime,CreatedAt,UpdatedAt) values('{}','{}','{}','{}','{}','{}',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)".format(data['Restaurant_AddressId '],data['Timings'][DAY]['Day'],data['Timings'][DAY]['Pickup']['Start'],data['Timings'][DAY]['Pickup']['End'],data['Timings'][DAY]['Delivary']['Start'],data['Timings'][DAY]['Delivary']['Day'] ))
                        logger.info("Inserted in timemaster")
            
            
            except:
                logger.error("ERROR: There is a problem in insert Time query")
                return {"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error in time query")}
                sys.exit()
            
            try:
                with conn.cursor() as curr:
                    if data['Payment_Option'] == 'all':
                        for i in id1:
                            curr.execute("insert into PaymentDetailMaster(PaymentDetail_Id,Restaurant_Id,PaymentType_Id) values(1,data[Restaurant_Id],{}).format(i)")
                    
                    else:
                        for i in data['Payment_Option']:
                            curr.execute("Select Payment_Id from PaymentMaster where Type = {}".format(i))
                            id = curr.fetchall()
                            Pay_Id = [i[0] for i in id]
                            curr.execute("insert into PaymentDetailMaster(PaymentDetail_Id,Restaurant_Id,PaymentType_Id) values(1,data[Restaurant_Id],{}).format(Pay_Id)")
               
               
                return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
                
                sys.exit()
                
            except:
                logger.error("ERROR: There is a problem in insert Payment query")
                return {"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error in insert ayment query")}
                sys.exit()
                
            conn.close()
    
    except:
        logger.error("ERROR: There is a problem in SQL query")
        return {"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error")}
        sys.exit()
        
        
        
        
        
        
        
        
        
        
        

