# This function is created for activating/deactivating items.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from pytz import timezone
import datetime
from thundra.thundra_agent import Thundra
thundra = Thundra()
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)

# Using logging module
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In Itempost Handler")
    print(event)
    item = json.loads(event['body'])
    print(item)
    logger.debug("Event Body: {}".format(item))
    
    try:
        with conn.cursor() as cur:
            cur.execute('Select Item_Status from FoodMaster where Item_Id={}'.format(item['Item_Id']))
            tup = cur.fetchall()
            logger.debug("Query Output: {}".format(tup))
            if (1,) == tup[0]:
                cur.execute('Update FoodMaster set Item_Status = "0",LastUpdate="{}" where Item_Id="{}"'.format(item['Item_Id'],date))
                conn.commit()
                
                # Close conn.cursor()
                cur.close()
            elif (0,) == tup[0]:
                cur.execute('Update FoodMaster set Item_Status = "1",LastUpdate="{}" where Item_Id="{}"'.format(item['Item_Id'],date))
                conn.commit()
                
                # Close conn.cursor()
            cur.close()
        return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
    except Exception as e:
        print(e)
        logger.error("ERROR: There is a problem in SQL ERROR")
        return{"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error")}
        sys.exit()

