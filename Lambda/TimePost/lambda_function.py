# This function is created for activating/deactivating orders also noting deactivated time.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()

# Using logging module.
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In TimePost handler")
    time = json.loads(event['body'])
    logger.debug("Event Body: {}".format(time))
    try:
        with conn.cursor() as cur:
            cur.execute('Select Status from TimeMaster where Time_Id="{}"'.format(time['timeid']))
            tup = cur.fetchall()
            logger.debug("Query Output: {}".format(tup))
            if (1,) == tup[0]:
                cur.execute('Update TimeMaster set Status = "0",UpdatedAt=CURRENT_TIMESTAMP where Time_Id="{}"'.format(time['timeid']))
                conn.commit()
                
                
            elif (0,) == tup[0]:
                cur.execute('Update TimeMaster set Status = "1",UpdatedAt=CURRENT_TIMESTAMP where Time_Id="{}"'.format(time['timeid']))
                conn.commit()
                
            cur.execute("select * from TimeMaster where Time_Id ={}".format(time['timeid']))
            out = cur.fetchall()
            # data = [i[0] for i in out]
            logger.debug("Output: {}".format(out))
            cur.close()
            if out[0][7] == 0:
                stat = "Deactive"
            else:
                stat = "Active"
        return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("status changed to {} ".format(stat))}
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query {}".format(e))
        return {"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error")}
        sys.exit()


