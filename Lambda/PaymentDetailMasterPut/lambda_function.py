# This function is created for inserting item data to ItemDuplicate and Item Table.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from pytz import timezone
import datetime
from thundra.thundra_agent import Thundra
thundra = Thundra()
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)

# Using logging module
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In Itemput handler")
    item = json.loads(event["body"])
    logger.debug("Event body: {}".format(item))
    try:
        with conn.cursor() as cur:
            cur.execute('select Restaurant_Id from RestaurantMaster where Name = "{}"'.format(item['name']))
            id = cur.fetchall()
            restaurantid = [i[0] for i in id]
            cur.execute('select Restaurant_AddressId from Restaurant_AddressMaster where Restaurant_Id = {} and Branch_Name = "{}"'.format(restaurantid[0],item['branchname']))
            RA = cur.fetchall()
			RAid = [i[0] for i in RA]
			logger.info("select RD id query executed")
            try:
				for i in range(0,len(item['payment']))  
					cur.execute('select Payment_Id from PaymentMaster where type = "{}"'.format(item['payment'][i]))
					id1 = cur.fetchall()
					paymentid = [i[0] for i in id1] 
					cur.execute('Insert into PaymentDetailMaster(Restaurant_Id,Restaurant_AddressId,PaymentType_Id,Status,UpdatedAt,CreatedAt) values({},{},{},1,"{}","{}")'.format(restaurantid[0],RAid[0],paymentid[0],date,date))
					conn.commit()
                logger.info("Insert into payment detail maste query executed")
			except Exception as e:
				logger.error(e)
            return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("success")}
            cur.close()
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query")
        logger.error(e)
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error in items")}
        sys.exit()
    

