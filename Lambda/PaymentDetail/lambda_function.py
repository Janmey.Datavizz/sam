# This function is created for activating/deactivating items.

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()

# Using logging module
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()
        
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
   
# Main Function
@thundra
def handler(event, context):
    logger.info("In Itempost Handler")          
    
    try:
        with conn.cursor() as cur: 
            
            restaurantaddress_id = event["queryStringParameters"]["restaurantaddress_id"]
            
            try:
                
                cur.execute("SELECT Restaurant_AddressMaster.Branch_Name,Restaurant_AddressMaster.Fullname,Restaurant_AddressMaster.Restaurant_AddressId,CustomerMaster.Cognito_Id,CustomerMaster.Customer_Id,OrderMaster.Type_Id,PaymentDetail.Payment_Id,PaymentDetail.Amount,PaymentDetail.Status,PaymentDetail.Transaction_Id FROM PaymentDetail INNER JOIN Restaurant_AddressMaster ON PaymentDetail.`Restaurant_AddressId`=Restaurant_AddressMaster.`Restaurant_AddressId` INNER JOIN `CustomerMaster` ON PaymentDetail.`Customer_Id`=CustomerMaster.`Customer_Id` INNER JOIN OrderMaster ON PaymentDetail.`Order_Id`=OrderMaster.`Order_Id` WHERE PaymentDetail.Restaurant_AddressId={}".format(restaurantaddress_id))
                tup = cur.fetchall()
            except Exception as e:
                logger.error("ERROR: There is a problem in SQL ERROR {}".format(e))
                
            try: 
                
                cur.execute("select TypeName from Order_TypeMaster where Order_TypeId = {}".format(tup[0][5]))
                typeid = cur.fetchall()
                typeId = [i[0]for i in typeid]
            except Exception as e:
                logger.error("ERROR: There is a problem in SQL {}".format(e))
                
            try:
                cur.execute("select Type from PaymentMaster where Payment_Id = {}".format(tup[0][6]))
                ptypeid = cur.fetchall()
                PtypeId = [i[0]for i in ptypeid]
            except Exception as e:
                logger.error("ERROR: There is a problem  {}".format(e))
                    
            logger.debug("Query Output: {},{},{}".format(tup,typeId[0],PtypeId[0]))
            holder = {}
            for row in range(0,len(tup)):
                order_details = {}
                order_details['BranchName'] = tup[row][0]
                order_details['Restaurant_Name'] = tup[row][1]
                order_details['Restaurant_AddressId'] =tup[row][2]
                # order_details['Payment'] =tup[row][3]
                order_details['CustomerId'] =tup[row][4]
                order_details['TypeId'] = tup[row][5]
                order_details['PaymentId'] = tup[row][6]
                order_details['Amount'] =tup[row][7]
                order_details['status'] = tup[row][8]
                order_details['TransactionId'] = tup[row][9]
                order_details['type'] = typeId[0]
                order_details['Payment Type'] = PtypeId[0]
                holder.update({row:order_details}) 
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            print (content)
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
            
            cur.close()
            return{"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps(out)}
    
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL ERROR {}".format(e))
        return{"statusCode":500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error")}
        sys.exit()
        
        
        
