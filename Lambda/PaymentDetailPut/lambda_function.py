# This function is created to get data from special_item_list Table

# Import external and built-in libraries 
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()
# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In Special Handler")
    try:
        with conn.cursor() as cur:
            cur.execute("Select special_item_id,special_item_name,restaurant_id,special_item_cost,category_id,stuffing_id,special_item_description,create_date,last_update,status,special_days from special_item_list")
            tup = cur.fetchall()
            logger.debug("Query Tuple: {}".format(tup))
            conn.commit()
            holder = {}
            for row in range(0,len(tup)):
                special_details = {}
                special_details['special_item_id'] = tup[row][0]
                special_details['special_item_name'] = tup[row][1]
                special_details['restaurant_id'] = tup[row][2]
                special_details['special_item_cost'] =tup[row][3]
                special_details['category_id'] =tup[row][4]
                special_details['stuffing_id'] = tup[row][5]
                special_details['special_item_description'] = tup[row][6]
                special_details['create_date'] = tup[row][7]
                special_details['last_update'] = tup[row][8]
                special_details['status'] = tup[row][9]
                special_details['special_days'] = tup[row][10]
                holder.update({row:special_details})
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            
            # Close conn.cursor()
            cur.close()
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
        logger.debug("Output JSON response: {}".format(out))
        return out
    except:
        logger.error("ERROR: There is a problem in SQL query")
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data")}
        sys.exit()
    
