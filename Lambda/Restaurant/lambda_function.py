# This function is created to get data from item_list Table

# Import external and built-in libraries
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()

# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In Item Handler")
    try:
        with conn.cursor() as cur:
            cur.execute("Select * from RestaurantMaster where Status = 1")
            tup = cur.fetchall()
            logger.debug("Query Output: {}".format(tup))
            conn.commit()
            holder = {}
            for row in range(0,len(tup)):
                item_details = {}
                item_details['Restaurant_Id'] = tup[row][0]
                item_details['Name'] = tup[row][1]
                item_details['Fullname'] = tup[row][2]
                item_details['Email'] =tup[row][3]
                item_details['Password'] =tup[row][4]
                item_details['Number'] = tup[row][5]
                item_details['GST_Id'] = tup[row][6]
                item_details['Business_Name'] = tup[row][7]
                item_details['Tax_Slab'] = tup[row][8]
                item_details['Status'] = tup[row][9]
                item_details['CreatedAt'] = tup[row][10]
                item_details['UpdatedAt'] = tup[row][11]
                holder.update({row:item_details})
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
            
            # Close conn.cursor()
            cur.close()
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
        logger.info("Output JSON response: {}".format(out))
        
        return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":out}
    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query {}".format(e))
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data {}".format(e))}
        sys.exit()


