# This function is created to get data from special_item_list Table

# Import external and built-in libraries 
import pymysql
import sys
import logging
import os
import json
from pytz import timezone
import datetime
from thundra.thundra_agent import Thundra
thundra = Thundra()

fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)

# Using logging library
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']



# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    logger.info("In Itemput handler")
    item = json.loads(event["body"])
    logger.debug("Event body: {}".format(item))
    try:
        with conn.cursor() as cur:
            
            cur.execute('select * from TypeMaster')
            print(cur.rowcount)
            if not cur.rowcount:
                cur.execute('Insert into TypeMaster(Name,Status,CreatedAt) values("{}",1,"{}")'.format(item['Name'],date))
                conn.commit()
                logger.info("Insert into TypeMaster Initially query executed")
                cur.execute('select max(Type_Id) from TypeMaster')
                tup5 = cur.fetchall()

                return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("new type added id {}".format(tup5[0][0]))}
            else:
                cur.execute('select max(Type_Id) from TypeMaster')
                tup4 = cur.fetchall()
                print(tup4[0][0])
                cur.execute('alter table TypeMaster AUTO_INCREMENT={}'.format(tup4[0][0]))
                conn.commit()
                cur.execute('Insert into TypeMaster(Name,Status,CreatedAt) values("{}",1,"{}")'.format(item['Name'],date))
                conn.commit()
                logger.info("Insert into TypeMaster Initially query executed")
                cur.execute('select max(Type_Id) from TypeMaster')
                tup6 = cur.fetchall()
                return {'statusCode':200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("new type added id {}".format(tup6[0][0]))}

    except Exception as e:
        logger.error("ERROR: There is a problem in SQL query")
        logger.error(e)
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("error in {}".format(e))}
        sys.exit()