# This function is created to fetch orders from order_list table

# Import external and built-in module
import pymysql
import sys
import logging
import os
import json
from thundra.thundra_agent import Thundra
thundra = Thundra()
# Using logging module
logger = logging.getLogger()
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

# Credentials of RDS host and stored in Environment Variables
rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']

# Connection with the database
try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=3268, connect_timeout=30)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")

# Main Function
@thundra
def handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    orderid = json.loads(event['queryStringParameters']['orderid'])
    logger.info("In Order handler")
    try:
        with conn.cursor() as cur:
            cur.execute('Select OLI.Order_Id,FM.Name,OLI.Quantity,OLI.Comments,OLI.Stuffing_Id from OrderItemList AS OLI INNER JOIN OrderMaster AS OM ON OLI.Order_Id=OM.Order_Id INNER JOIN FoodMaster AS FM ON OLI.Item_Id= FM.Item_Id WHERE OM.Order_Id="{}"'.format(orderid))
            tup = cur.fetchall()
            logger.debug("Query Output1: {}".format(tup))
            holder = {}
            for row in range(0,len(tup)):
                order_details = {}
                order_details['Item_Name'] = tup[row][1]
                order_details['Quantity'] =tup[row][2]
                order_details['Comments'] =tup[row][3]
                order_details['Stuffing_Name'] =tup[row][4]
                holder.update({row:order_details})
            holder1 = json.dumps(holder,default =str)
            content = json.loads(holder1)
        
            cur.close()
            out = {}
            if len(content) > 0:
                out['statusCode'] = 200
                out['body'] = content
            else:
                out['statusCode'] = 201
                out['body'] = "NO DATA"
        return {"statusCode":200,"headers":{"access-control-allow-origin":"*"},"body":json.dumps(out)}
    except:
        logger.error("ERROR: There is a problem in SQL query")
        return {'statusCode':500,"headers":{"access-control-allow-origin":"*"},"body":json.dumps("Error Fetching Data")}
        sys.exit()