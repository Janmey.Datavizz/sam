import pymysql
import sys
import logging
import os
import datetime
from pytz import timezone

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

rds_host  = os.environ['rds_host']
name = os.environ['name']
password = os.environ['password']
db_name = os.environ['db_name']
restaurant_name = os.environ['restaurant']
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_utc = datetime.datetime.now(timezone('Asia/Kolkata'))
date = now_utc.strftime(fmt)

try:
        conn = pymysql.connect(host= rds_host, user=name, password=password, database=db_name, port=3268, connect_timeout=600)
except:
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        sys.exit()
        
        logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
        """
        This function fetches content from mysql RDS instance
        """
        item_count = 0

class db:

    def handle2(event,dish,quantity,stuffing,cost,payment_value,ordertype,branch,cognito_id,alexauserid):
        with conn.cursor() as cur:
            logger.debug("{}".format(dish))
            logger.debug("{}".format(quantity))
            logger.debug("{}".format(stuffing))
            logger.debug("{}".format(cost)) 
            logger.debug("{}".format(branch))
            logger.debug("{}".format(cognito_id))
            logger.debug("{}".format(alexauserid))
            ItemId= []
            TypeId= [] 
            StuffingId= []
            cur.execute("select Customer_Id from CustomerMaster where Cognito_Id='{}'".format(cognito_id))
            customertup = cur.fetchall()
            customerid = [i[0] for i in customertup]
            if len(customertup) == 0:
                try:
                    cur.execute("Insert into CustomerMaster(Cognito_Id,User_Id) values('{}','{}')".format(cognito_id,alexauserid))
                    conn.commit()
                    try:
                        cur.execute("select max(Customer_Id) from CustomerMaster")
                        id = cur.fetchall()
                        id = [i[0] for i in id]
                        CustomerId = id[0]
                    except Exception as e:
                        logger.error("{}".format(e))
                except Exception as e:
                    logger.error("{}".format(e))
            else:
                CustomerId = customerid[0]
            cur.execute("select Order_TypeId from Order_TypeMaster where TypeName='{}'".format(ordertype))
            ordertypetup = cur.fetchall()
            if payment_value:
                cur.execute("select Payment_Id from PaymentMaster where Type='{}'".format(payment_value))
                paymenttup = cur.fetchall()
                payment1 = [i[0] for i in paymenttup]
                payment = payment1[0]
    
            cur.execute("select Restaurant_AddressId from Restaurant_AddressMaster where Branch_Name='{}'".format(branch))
            branchtup = cur.fetchall()
            for i in range(0,len(dish)):
                cur.execute("select Item_Id,Stuffing_Id from FoodMaster where Name = '%s'"%(dish[i]))
                tup = cur.fetchall()
                itemid = [i[0] for i in tup]
                ItemId.append(itemid[0])
                # typeid= [i[1] for i in tup]
                # TypeId.append(typeid[0])
                stuffingid= [i[1] for i in tup]
                StuffingId.append(stuffingid[0])
            cur.execute("select * from OrderMaster")
            if not cur.rowcount:
                cur.execute("insert into OrderMaster(Customer_Id,Branch_Id,Status_Id,Payment_Id,Type_Id,CreatedAt,AcceptedAt,DeliveredAt,Coupon_Id) values({},{},1,{},{},'{}','{}','{}',1)".format(CustomerId,branchtup[0][0],payment,ordertypetup[0][0],date,date,date))
                conn.commit()
                cur.execute('select max(Order_Id) from OrderMaster where Customer_Id={}'.format(CustomerId))
                tup4 = cur.fetchall()
                Order_Id = tup4[0][0]
            else:
                cur.execute("select max(Order_Id) from OrderMaster")
                tup3 = cur.fetchall()
                cur.execute('alter table OrderMaster AUTO_INCREMENT={}'.format(tup3[0][0]))
                conn.commit()
                cur.execute("insert into OrderMaster(Customer_Id,Branch_Id,Status_Id,Payment_Id,Type_Id,CreatedAt,AcceptedAt,DeliveredAt,Coupon_Id) values({},{},1,{},{},'{}','{}','{}',1)".format(CustomerId,branchtup[0][0],payment,ordertypetup[0][0],date,date,date))
                cur.execute('select max(Order_Id) from OrderMaster where Customer_Id = {}'.format(CustomerId))
                tup4 = cur.fetchall()
                Order_Id = tup4[0][0]
            for i in range(0,len(dish)):
                if stuffing[i] == '0':
                    logger.debug("this is order id {}".format(Order_Id))
                    logger.debug("this is item id {}".format(ItemId[i]))
                    logger.debug("this is quantity id {}".format(quantity[i]))
                    cur.execute("select * from OrderItemList")
                    if not cur.rowcount:
                        cur.execute("insert into OrderItemList(Order_Id,Item_Id,Quantity,Comments) values({},{},{},'nothing')".format(Order_Id,ItemId[i],quantity[i]))
                        conn.commit()
                        logger.debug("Insert first OrderMaster Query executed")
                    else:
                        try:
                            cur.execute("select max(OrderItemList_Id) from OrderItemList")
                            tup2 = cur.fetchall()
                            cur.execute('alter table OrderItemList AUTO_INCREMENT={}'.format(tup2[0][0]))
                            conn.commit()
                            cur.execute("insert into OrderItemList(Order_Id,Item_Id,Quantity,Comments) values({},{},{},'nothing')".format(Order_Id,ItemId[i],quantity[i]))
                            logger.info("Insert second OrderMaster Query executed")
                            conn.commit()
                        except Exception as e:
                            logger.error("{}".format(e))
                else:
                    cur.execute("select * from OrderItemList")
                    if not cur.rowcount:
                        cur.execute("insert into OrderItemList(Order_Id,Item_Id,Quantity,Stuffing_Id,Comments) values({},{},{},{},'nothing')".format(Order_Id,ItemId[i],quantity[i],StuffingId[i]))
                        conn.commit()
                        logger.info("Insert first OrderItemList with Stuffing query executed")
                    else:
                        cur.execute("select max(OrderItemList_Id) from OrderItemList")
                        tup1 = cur.fetchall()
                        cur.execute('alter table OrderItemList AUTO_INCREMENT={}'.format(tup1[0][0]))
                        conn.commit()
                        cur.execute("insert into OrderItemList(Order_Id,Item_Id,Quantity,Stuffing_Id,Comments) values({},{},{},{},'nothing')".format(Order_Id,ItemId[i],quantity[i],StuffingId[i]))
                        logger.info("Insert Second OrderItemList with Stuffing query executed")
                        conn.commit()    
            cur.close()
            
    def handle7(event,dish_value):
        with conn.cursor() as cur:
            cur.execute("Select GROUP_CONCAT(ISM.Name SEPARATOR ', ') from FoodMaster INNER JOIN StuffingMaster AS ISM ON FoodMaster.Stuffing_Id=ISM.Stuffing_Id where Item_Number ='%s'"%(dish_value))  
            var = cur.fetchall()
            cur.close()
            return var
    
    def handle8(event,dish_value):
        with conn.cursor() as cur:
            cur.execute("Select Type_Id from FoodMaster where Item_Number ='%s'"%(dish_value)) 
            var = cur.fetchall()
            cur.close()
            return var
           
    def handle9(event,dish_value):
        with conn.cursor() as cur:
            if " " in dish_value:
                cur.execute("Select Item_Number from FoodMaster where Name = '%s'"%(dish_value))  
                var = cur.fetchall()
                logger.debug("{}".format(var))
            else:
                if dish_value.isdigit() == True:
                    dish = int(dish_value)
                    cur.execute("Select Item_Number from FoodMaster where Item_Number = %d"%(dish))
                    var = cur.fetchall()
                    logger.debug("{}".format(var))
                else:
                    cur.execute("Select Item_Number from FoodMaster where Name = '%s'"%(dish_value))
                    var = cur.fetchall()
                    logger.debug("{}".format(var))
            # conn.commit()
            cur.close()
            return var
            
    def handle13(event,restaurant,branch):
        with conn.cursor() as cur:
            cur.execute("select Restaurant_Id from RestaurantMaster where Name='{}' and Status='1'".format(restaurant))
            res = cur.fetchall()
            cur.execute("select Restaurant_AddressId from Restaurant_AddressMaster where Branch_Name='{}' and Restaurant_Id='{}' and Status=1".format(branch,res[0][0])) 
            tup = cur.fetchall()
            cur.close()
            return tup

    def handle14(event,cognito_id):
        with conn.cursor() as cur:
            cur.execute("select Customer_Id from CustomerMaster where Cognito_Id='{}'".format(cognito_id))
            tup = cur.fetchall()
            cur.close()
            return tup

    def handle15(event,dish,quantity):
        with conn.cursor() as cur:
            try:
                cur.execute("select Tax_Slab,Restaurant_Id from RestaurantMaster where Name = '{}'".format(restaurant_name))
                data = cur.fetchall()
                detail = [i[0] for i in data]
                Restaurant_Id = detail[0]
                logger.info("this is Tax_Slab and id of kbobs '{}'".format(detail)) 

                try: 
                    cur.execute("select Cost from FoodMaster where Name = '{}'".format(dish[-1]))
                    var = cur.fetchall()
                    cos = [i[0] for i in var]
                    Tax_Slab = detail[0]
                    cost = ((cos[0]*quantity[-1])*(Tax_Slab)/100)+(cos[0]*quantity[-1])
                    return cost
                except Exception as e:
                    logger.error("{}".format(e))
                cur.close()

            except Exception as e:
                logger.error("{}".format(e))
            cur.close()
            
    def handle16(event):
        with conn.cursor() as cur:
            try:
                cur.execute("select Branch_Name from Restaurant_AddressMaster where Delivery_Option = 1 or Delivery_Option=3 and Status='1'")
                tup = cur.fetchall()
                cur.close()
                return tup
            except Exception as e:
                print e
            
    def handle17(event,lat,longitude):
        with conn.cursor() as cur:
            try:
                for i in range(0,len(lat)):
                    lat = str(lat[i])
                    longitude = str(longitude[i])
                    latitude=lat[:6]
                    longitude1=longitude[:6]
                    cur.execute("select Branch_Name from Restaurant_AddressMaster where Latitude like '{}%' and Longitude like '{}%'".format(latitude,longitude1))
                    tup = cur.fetchall()
                    return tup
            except Exception as e:
                logger.error("{}".format(e))
            cur.close()
            
    def handle18(event):
        with conn.cursor() as cur:
            try:
                cur.execute("select Delivery_Radius from Restaurant_AddressMaster where Fullname = '{}'".format(restaurant_name)) 
                tup = cur.fetchall()
                radius = [i[0] for i in tup]
                return radius
                # conn.commit()
            except Exception as e:
                logger.error("error in fetching '{}'".format(e))
            cur.close()

    def handle19(event,dish_value):
        with conn.cursor() as cur:
            try:
                cur.execute("Select Item_Status from FoodMaster where Item_Number = {}".format(dish_value))  
                var = cur.fetchall()
                return var
            except Exception as e:
                logger.error("error in fetching '{}'".format(e))
            cur.close()
    
    def handle20(event,restaurant,branch):
        with conn.cursor() as cur:
            try:
                cur.execute("Select Restaurant_Id from RestaurantMaster where Name = '{}'".format(restaurant))  
                var = cur.fetchall()
                cur.execute("Select Restaurant_AddressId from Restaurant_AddressMaster where Restaurant_Id={} and Branch_Name='{}'".format(var[0][0],branch))
                second = cur.fetchall()
                cur.execute("Select PM.Type from PaymentDetailMaster AS PDM INNER JOIN PaymentMaster AS PM ON PM.Payment_Id = PDM.PaymentType_Id where Restaurant_AddressId={}".format(second[0][0]))
                third = cur.fetchall()
                return third
            except Exception as e:
                logger.error("error in fetching '{}'".format(e))
            cur.close()