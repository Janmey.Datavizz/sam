import logging
import requests
import pymysql
import sys
from database import db
from geofencing import branch
import math
import datetime
import os
import json

from pubnub.pnconfiguration import PNConfiguration
from pubnub.pubnub import PubNub
from pubnub.exceptions import PubNubException
from ask_sdk_model import ui
from ask_sdk_model.ui import SimpleCard,StandardCard
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_core.dispatch_components import (
    AbstractRequestHandler, AbstractExceptionHandler,
    AbstractResponseInterceptor, AbstractRequestInterceptor)
from ask_sdk_core.utils import is_intent_name, is_request_type
from typing import Union, Dict, Any, List
from ask_sdk_model.dialog import (
    ElicitSlotDirective, DelegateDirective,ConfirmIntentDirective)
from ask_sdk_model import (
    Response, IntentRequest, DialogState, SlotConfirmationStatus, IntentConfirmationStatus, Slot)
from ask_sdk_model.slu.entityresolution import StatusCode
from ask_sdk_model.interfaces.alexa.presentation.apl import (
    RenderDocumentDirective, ExecuteCommandsDirective, SpeakItemCommand,
    AutoPageCommand, HighlightMode)
from ask_sdk_model.interfaces.display import (
    ImageInstance, Image,
    BackButtonBehavior, BodyTemplate2,RenderTemplateDirective)    
now =  datetime.datetime.now()
restaurant = os.environ['restaurant']
loggerlevel = os.environ['loggerlevel'] 
cognitoapi = os.environ['cognitoapi']
paytmapi = os.environ['paytmapi']
subscribe_key = os.environ['subscribe_key']
channel = os.environ['channel']
publish_key = os.environ['publish_key']
secret_key = os.environ['secret_key']
auth_key = os.environ['auth_key']
launchcard = os.environ['launchcard']
launchmessage = os.environ['launchmessage']

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

indent_slots = {
    'dish_value':'dish',
    'quantity_value' :'one',
    'table_value': 'tablenumber',
    'note_value' : 'note',  
    'subcategory':'subcategory',
    'subcategory_slot':'subcategory',
    'rep_dish_slot':'rep_dish',
    'final_dish_slot':'final_dish',
    'paymentmode':'paymentmode',
    'ordertype':'ordertype',
    'branch':'branch'
}

main1 = db()  
main2 = branch()
dish = []
quantity = []
cost = []
stuffing = []
branchval = []

class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for skill launch."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_request_type("LaunchRequest")(handler_input)
        
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response   
        logger.debug("In LaunchRequestHandler")
        accesstoken= handler_input.request_envelope.context.system.user.access_token
        logger.debug("{}".format(accesstoken))
        if accesstoken:
            handler_input.attributes_manager.session_attributes["access_token"] = accesstoken
            headers={'Authorization':"Bearer "+accesstoken}
            r = requests.get("{}".format(cognitoapi),headers=headers)
            data = r.json()
            logger.debug("{}".format(data))  
            handler_input.attributes_manager.session_attributes["data"] = data
            speech = "{}".format(launchmessage)
            reprompt = "{}".format(launchmessage)
            speech_text = "{}".format(launchcard)
            handler_input.response_builder.speak(speech).ask(reprompt).set_card(
                SimpleCard(restaurant, speech_text)).set_should_end_session(False)
        else:
            speech=("Account linking is required to use this skill.")
            handler_input.response_builder.speak(speech).set_card(
            SimpleCard(restaurant, speech)).set_should_end_session(True)
        return handler_input.response_builder.response

class TypeOfOrderHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
         return is_intent_name("TypeOfOrder")(handler_input)
    
    def handle(self, handler_input):
        logger.debug("In TypeOfOrderHandler")
        accesstoken= handler_input.request_envelope.context.system.user.access_token
        handler_input.attributes_manager.session_attributes["access_token"] = accesstoken
        headers={'Authorization':"Bearer "+accesstoken}
        r = requests.get("{}".format(cognitoapi),headers=headers)
        data = r.json()
        handler_input.attributes_manager.session_attributes["data"] = data
        del dish[:]
        del quantity[:]
        del stuffing[:]
        del cost[:] 
        slots = handler_input.request_envelope.request.intent.slots
        ordertype=slots[indent_slots['ordertype']].value
        current_intent = handler_input.request_envelope.request.intent
        handler_input.attributes_manager.session_attributes["ordertype"] = ordertype
        if accesstoken: 
            if ordertype == "pick up":
                var = main1.handle16()
                branch = [i[0] for i in var]
                handler_input.attributes_manager.session_attributes["validatebranch"] = branch
                speech = "Select from the following branches {}. Say branch name to schedule your pick up?".format(var)
                reprompt = "Select from the following branches {}. Say branch name to schedule your pick up?".format(var)
                handler_input.response_builder.speak(speech).ask(reprompt).set_card(
                        SimpleCard(restaurant, speech)).set_should_end_session(False)
                return handler_input.response_builder.response
            elif ordertype == "delivery":
                lat = data['custom:latitude']
                lon = data['custom:longitude']
                var = main2.getbranch(lat,lon)
                handler_input.attributes_manager.session_attributes["validatebranch"] = var
                
                if len(var) == 1:
                    speech = "Only branch {} is delivering to your location. Say {} to start order.".format(var,var)
                    reprompt = "Only branch {} is delivering to your location. Say {} to start order.".format(var,var)
                    handler_input.response_builder.speak(speech).ask(reprompt).set_card(
                        SimpleCard(restaurant, speech)).set_should_end_session(False)
                    return handler_input.response_builder.response
                elif var == 0:
                    speech = "Sorry currently none of our branch is delivering to your location."
                    handler_input.response_builder.speak(speech).set_card(
                        SimpleCard(restaurant, speech)).set_should_end_session(True)
                    return handler_input.response_builder.response
                else:
                    speech = "Which from the following branch you want to order? Your nearest branches are {}.".format(var)
                    reprompt = "Which from the following branch you want to order? Your nearest branches are {}.".format(var)
                    handler_input.response_builder.speak(speech).ask(reprompt).set_card( 
                        SimpleCard(restaurant, speech)).set_should_end_session(False)
                    return handler_input.response_builder.response
            return handler_input.response_builder.add_directive(DelegateDirective(updated_intent=current_intent)).response
        else:
            speech=("Account linking is required to use this skill.")
            handler_input.response_builder.speak(speech).set_card(
            SimpleCard(restaurant, speech)).set_should_end_session(True)
            return handler_input.response_builder.response

#handler that validates the item name user wants to order and checks for the stuffing and type
class dummyHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
         return is_intent_name("dummy")(handler_input)

    def handle(self, handler_input):
        logger.debug("In dummyHandler")
        branchslot = handler_input.request_envelope.request.intent.slots[indent_slots['branch']].value
        handler_input.attributes_manager.session_attributes["branch"] = branchslot
        validatebranch = handler_input.attributes_manager.session_attributes["validatebranch"]
        branchval.append(branchslot)
        ordertype = handler_input.attributes_manager.session_attributes["ordertype"]
        accesstoken = handler_input.attributes_manager.session_attributes["access_token"]
        if ordertype == "delivery" and branchval[0] not in validatebranch:
            speech = "only branch {} is available. please choose from that".format(validatebranch)
            reprompt ="only branch {} is available. please choose from that".format(validatebranch)
            handler_input.response_builder.speak(speech).ask(reprompt).set_should_end_session(False)
            return handler_input.response_builder.response
        current_intent = handler_input.request_envelope.request.intent
        slots = handler_input.request_envelope.request.intent.slots[indent_slots['dish_value']].confirmation_status
        dish_val = handler_input.request_envelope.request.intent.slots
        dish_value = dish_val[indent_slots['dish_value']].value
                  
        if dish_value:
            slots = handler_input.request_envelope.request.intent.slots
            handler_input.attributes_manager.session_attributes["dish_name"] = dish_value
            variab = main1.handle9(dish_value)
            li = [i[0] for i in variab]
            if len(li) == 0:
                speech = ("{} is not the correct dish name, please say the correct name.").format(dish_value)
                reprompt = ("{} is not the correct dish name, please say the correct name.").format(dish_value)
                handler_input.response_builder.speak(speech).ask(reprompt).set_card(
                SimpleCard(restaurant, speech)).set_should_end_session(False)
                return handler_input.response_builder.response
                        
            variable = main1.handle19(li[0])
            last = [i[0] for i in variable]
            if last[0] == 0:
                speech = ("Sorry {} is out of stock. please order any other item ").format(dish_value)
                reprompt = ("Sorry {} is out of stock. please order any other item ").format(dish_value)
                handler_input.response_builder.speak(speech).ask(reprompt).set_card(
                SimpleCard(restaurant, speech)).set_should_end_session(False)
                return handler_input.response_builder.response
                            
            iddish = li[0]
            handler_input.attributes_manager.session_attributes["dish_id"] = iddish    
            var = main1.handle7(li[0])
            varx = main1.handle8(li[0])
            li = [i[0] for i in var]
            lo = [i[0] for i in varx]
            if li[0] != None:
                speech = ("Sure ,which of the stuffing would you like to choose?{}").format(var)
                reprompt = ("Sure ,which of the stuffing would you like to choose?{}").format(var)
                handler_input.response_builder.speak(speech).ask(reprompt)
                return handler_input.response_builder.response                
            else:
                speech = "May I know the quantity?"
                reprompt ="okay, please tell me how much quantity you would like to order?"
                handler_input.response_builder.speak(speech).ask(reprompt)
                return handler_input.response_builder.response                    
        return handler_input.response_builder.add_directive(DelegateDirective(updated_intent=current_intent)).response

class OkayOrderHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        return (is_intent_name("okayorder")(handler_input) and handler_input.request_envelope.request.dialog_state != DialogState.COMPLETED)
    
    def handle(self, handler_input):
        logger.debug("In orderIntent")
        current_intent = handler_input.request_envelope.request.intent
        intent_value = handler_input.request_envelope.request.intent.confirmation_status
        slots = handler_input.request_envelope.request.intent.slots
        dish_value = handler_input.attributes_manager.session_attributes["dish_name"] 
        dishid = handler_input.attributes_manager.session_attributes["dish_id"]
        current_slot = current_intent.slots
        var = main1.handle7(dishid)
        li = [i[0] for i in var]
        if li[0] != None:
            stuffing_value = handler_input.attributes_manager.session_attributes["stuffing"]
        else:
            stuffing_value = "0"
        varx = main1.handle8(dishid)
        lo = [i[0] for i in varx]
        if lo[0] == None:
            type_value = handler_input.attributes_manager.session_attributes["type"]
        else:
            type_value = "{}".format(lo[0])

        if intent_value == IntentConfirmationStatus.CONFIRMED:
            quantity_value = slots[indent_slots['quantity_value']].value
            dish.append(str(dish_value))
            stuffing.append(str(stuffing_value))  
            quantity.append(int(quantity_value))
            amount = main1.handle15(dish,quantity) 
            cost.append(amount)
            speech = ("Which item?" )
            reprompt = "Please say the correct dish name?" 
            handler_input.response_builder.speak(speech).ask(reprompt).set_card(
            SimpleCard(restaurant, speech)).set_should_end_session(False)
            return handler_input.response_builder.response
        
        elif intent_value == IntentConfirmationStatus.DENIED:
            quantity_value = slots[indent_slots['quantity_value']].value
            dish.append(str(dish_value))
            stuffing.append(str(stuffing_value))
            quantity.append(int(quantity_value))
            amount = main1.handle15(dish,quantity)
            cost.append(amount)
            items = []
            for i in range(0,len(dish)):
                items.append(quantity[i])
                items.append(dish[i])
            speech = ("<speak>Thank you. Your order is {}, bill amount with GST is <say-as interpret-as='cardinal'>{}</say-as> rupees. " \
                        "Do you confirm this?</speak>").format(items,int(sum(cost)))
            reprompt = "Please tell me should I confirm this? " \
                        "say yes or say cancel"
            speech_text = ("Thank you. Your order is {}, bill amount with GST is {} rupees. " \
                        "Do you confirm this?").format(items,int(sum(cost)))    
            handler_input.response_builder.speak(speech).ask(reprompt).set_card(
                    SimpleCard(restaurant, speech_text)).set_should_end_session(False)
            return handler_input.response_builder.response
        return handler_input.response_builder.add_directive(DelegateDirective(updated_intent=current_intent)).response
             
class AmazonNoHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        return is_intent_name("NoIntent")(handler_input)
        
    def handle(self, handler_input):
        logger.debug("In NoIntentHandler")
        speech = "Say cancel to cancel your order." 
        reprompt = "Say cancel to cancel your order."
        handler_input.response_builder.speak(speech).ask(reprompt).set_card(
                    SimpleCard("KBOB'S", speech)).set_should_end_session(False)
        return handler_input.response_builder.response

class AmazonYesHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
         return is_intent_name("YesIntent")(handler_input)

    def handle(self, handler_input):
        logger.debug("In yesHandler")
        data = handler_input.attributes_manager.session_attributes["data"] 
        ordertype = handler_input.attributes_manager.session_attributes["ordertype"]
        branch = handler_input.attributes_manager.session_attributes["branch"]
        if ordertype == "delivery": 
            speech = "Say confirm if your delivery address is {}.".format(data['address'])
            reprompt = "Please tell me confirm if your delivery address is correct " 
            handler_input.response_builder.speak(speech).ask(reprompt).set_card(
            SimpleCard(restaurant, speech)).set_should_end_session(False)
        elif ordertype == "pick up":
            dish_value = handler_input.attributes_manager.session_attributes["dish_name"]
            if len(dish_value)!=0:
                if len(quantity)!=0:
                    payment_value = "cash on delivery"
                    cognito_id = data['username']
                    alexauserid = handler_input.request_envelope.context.system.user.user_id
                    main1.handle2(dish,quantity,stuffing,cost,payment_value,ordertype,branch,cognito_id,alexauserid)
                    pnconfig = PNConfiguration()
                    pnconfig.subscribe_key = "{}".format(subscribe_key)
                    pnconfig.publish_key = "{}".format(publish_key)
                    pnconfig.secret_key = "{}".format(secret_key)
                    pnconfig.auth_key = "{}".format(auth_key)
                    pnconfig.uuid = alexauserid
                    pnconfig.ssl = False
                    pubnub = PubNub(pnconfig)
                    pubnub.subscribe().channels('{}'.format(channel)).execute()
                    envelope = pubnub.publish().channel("{}".format(channel)).message({
                    'name': 'New Order has arrived',
                    'online': True
                    }).sync()
                    del dish[:]
                    del quantity[:]
                    del stuffing[:]
                    del cost[:]
                    speech = ("Okay! You will be notified once your order is accepted.")
                    handler_input.response_builder.speak(speech).set_card(
                    SimpleCard(restaurant, speech)).set_should_end_session(True)
                else:
                    speech = ("How much quantity you want?")
                    handler_input.response_builder.speak(speech).set_card(
                    SimpleCard(restaurant, speech)).set_should_end_session(False)
                
            else:
                speech = ("Which item you want to order?")
                handler_input.response_builder.speak(speech).set_card(
                SimpleCard(restaurant, speech)).set_should_end_session(True)    
        return handler_input.response_builder.response
    
class ConfirmIntentHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
         return is_intent_name("ConfirmIntent")(handler_input)
    
    def handle(self, handler_input):
        branch = handler_input.attributes_manager.session_attributes["branch"]
        paymenttype = main1.handle20(restaurant,branch) 
        speech = "Which mode of payment do you prefer among {}?".format(paymenttype)
        reprompt = "Please tell me what would you choose cash on delivery or paytm"
        handler_input.response_builder.speak(speech).ask(reprompt).set_card(
        SimpleCard(restaurant, speech)).set_should_end_session(False)
        return handler_input.response_builder.response   
            
class PaymentHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
         return is_intent_name("modeofpayment")(handler_input)
    
    def handle(self, handler_input):
        logger.debug("In PaymentHandler")
        data = handler_input.attributes_manager.session_attributes["data"] 
        amount = sum(cost)
        slots = handler_input.request_envelope.request.intent.slots
        payment_value = slots[indent_slots['paymentmode']].value
        if payment_value == "cash on delivery":
            speech = ("You will be notified once your order is accepted.")
            handler_input.response_builder.speak(speech).set_card(
            SimpleCard(restaurant, speech)).set_should_end_session(True)
        elif payment_value == "paytm":
            email = data["email"]
            phone_number = data["phone_number"]
            phone_number = phone_number[3:]  
            branch = handler_input.attributes_manager.session_attributes["branch"]
            name = data["name"]
            cognito_id = data['username']
            restaurantin = main1.handle13(restaurant,branch)
            restaurantid = [i[0] for i in restaurantin]
            customerid = main1.handle14(cognito_id)
            params = { 
                "RestaurantAddress_Id":restaurantid[0],
                "Customer_Id":customerid,
                "Email":email,   
                "Mobile":phone_number,
                "Name":name,
                "amount":amount
            }
            r = requests.post("{}".format(paytmapi),data=json.dumps(params),headers={"Content-Type":"application/json"})
            paytmdata = r.json() 
            logger.debug("{}".format(paytmdata))
            speech = "Payment link is sent to your registered phone number. You will be notified once your order is accepted."
            handler_input.response_builder.speak(speech).set_card(
            SimpleCard(restaurant, speech)).set_should_end_session(True)
        data = handler_input.attributes_manager.session_attributes["data"] 
        ordertype = handler_input.attributes_manager.session_attributes["ordertype"]
        branch = handler_input.attributes_manager.session_attributes["branch"]
        alexauserid = handler_input.request_envelope.context.system.user.user_id
        cognito_id = data['username']
        main1.handle2(dish,quantity,stuffing,cost,payment_value,ordertype,branch,cognito_id,alexauserid)
        pnconfig = PNConfiguration()
        pnconfig.subscribe_key = "{}".format(subscribe_key)
        pnconfig.publish_key = "{}".format(publish_key)
        pnconfig.secret_key = "{}".format(secret_key)
        pnconfig.auth_key = "{}".format(auth_key)
        pnconfig.uuid = alexauserid
        pnconfig.ssl = False
        pubnub = PubNub(pnconfig)
        pubnub.subscribe().channels('{}'.format(channel)).execute()
        envelope = pubnub.publish().channel("{}".format(channel)).message({
        'msg': 'New order has arrived.',
        'online': True
        }).sync()    
        del dish[:]
        del quantity[:]
        del stuffing[:]
        del cost[:]
        return handler_input.response_builder.response   
    
class FallbackIntentHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("AMAZON.FallbackIntent")(handler_input)
    
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In FallbackIntentHandler")
        speech = ("Hmm Sorry I don't quite understand that. Try again by saying place an order.")
        reprompt = "Hmm Sorry I don't quite understand that. Try again by saying place an order." 
        handler_input.response_builder.speak(speech).ask(reprompt).set_card(SimpleCard(restaurant,speech)).set_should_end_session(False)
        return handler_input.response_builder.response
        
class HelpIntentHandler(AbstractRequestHandler):
    """Handler for help intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("AMAZON.HelpIntent")(handler_input)
    
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In HelpIntentHandler")
        speech = ("I can help you to place an order. Please say place an order.")
        reprompt = "I can help you to place an order. Please say place an order."
        handler_input.response_builder.speak(speech).ask(reprompt).set_card(SimpleCard(restaurant,speech)).set_should_end_session(False)
        return handler_input.response_builder.response

class ExitIntentHandler(AbstractRequestHandler):
    """Single Handler for Cancel, Stop and Pause intents."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("AMAZON.StopIntent")(handler_input)
    
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In ExitIntentHandler")
        handler_input.response_builder.speak("Bye").set_should_end_session(
            True)
        return handler_input.response_builder.response

class SessionEndedRequestHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_request_type("SessionEndedRequest")(handler_input)
    
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In SessionEndedRequestHandler")
        logger.info("Session ended with reason: {}".format(
            handler_input.request_envelope.request.reason))
        return handler_input.response_builder.response

# Exception Handler classes
class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Catch All Exception handler.
    This handler catches all kinds of exceptions and prints
    the stack trace on AWS Cloudwatch with the request envelope."""
    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True
    
    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        
        logger.error(exception, exc_info=True)
        if len(dish) > len(quantity):
            dish.pop()
        else: 
            speech = "Please say place an order"
        handler_input.response_builder.speak(speech).ask(speech).set_should_end_session(False)
        return handler_input.response_builder.response
        
class RequestLogger(AbstractRequestInterceptor):
    """Log the request envelope."""
    def process(self, handler_input):
        # type: (HandlerInput) -> None
        logger.info("Request Envelope: {}".format(
            handler_input.request_envelope))

class ResponseLogger(AbstractResponseInterceptor):
    """Log the response envelope."""
    def process(self, handler_input, response):
        # type: (HandlerInput, Response) -> None
        logger.info("Response: {}".format(response))
                
class stuffingHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
         return is_intent_name("stuffing")(handler_input)
    
    def handle(self, handler_input):
        logger.debug("In stuffingHandler")
        stuffing_value = handler_input.request_envelope.request.intent.slots["stuffings"].value
        logger.info("stuffing selected is {}".format(stuffing_value))
        dish_name =handler_input.attributes_manager.session_attributes["dish_name"]
        # store the value of stuffing in session
        handler_input.attributes_manager.session_attributes["stuffing"] = stuffing_value
        idvalue = handler_input.attributes_manager.session_attributes["dish_id"]
        var = main1.handle8(idvalue)
        li = [i[0] for i in var]
        logger.info("dish type is {}".format(li[0]))
        speech = "May I know the quantity?"
        reprompt = "May I know the quantity?"
        handler_input.response_builder.speak(speech).ask(reprompt).set_card(
                     SimpleCard(restaurant, speech)).set_should_end_session(True)
                     
        return handler_input.response_builder.response
                
class AmazonCancelHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
         return is_intent_name("AMAZON.CancelIntent")(handler_input)
    
    def handle(self, handler_input):
        logger.debug("In AmazonCancelHandler")
        if len(dish)!=0 and len(quantity)!=0:
            speech = ("Okay. Your order is cancelled. Have a good day!")
            reprompt = "Okay. Your order is cancelled. Have a good day!"
            handler_input.response_builder.speak(speech).set_card(
                     SimpleCard(restaurant, speech)).set_should_end_session(True)
                     
        elif len(dish) == 0 and len(quantity)==0:
            speech = ("You have not ordered anything yet. Have a good day!")
            reprompt = "You have not ordered anything yet. Have a good day!"
            handler_input.response_builder.speak(speech).set_card(
                     SimpleCard(restaurant, speech)).set_should_end_session(True)
        return handler_input.response_builder.response
sb = SkillBuilder()
sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(OkayOrderHandler())
sb.add_request_handler(dummyHandler())
sb.add_request_handler(stuffingHandler())
sb.add_request_handler(AmazonYesHandler())
sb.add_request_handler(AmazonNoHandler())
sb.add_request_handler(AmazonCancelHandler())
sb.add_request_handler(PaymentHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(FallbackIntentHandler())
sb.add_request_handler(ExitIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())
sb.add_request_handler(ConfirmIntentHandler())
sb.add_request_handler(TypeOfOrderHandler())
sb.add_exception_handler(CatchAllExceptionHandler())
sb.add_global_request_interceptor(RequestLogger())
sb.add_global_response_interceptor(ResponseLogger())
lambda_handler = sb.lambda_handler()