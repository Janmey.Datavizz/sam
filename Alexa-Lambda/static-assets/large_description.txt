KBOB'S lets you order from a full menu of Punjabi and related cuisines with Alexa and create new orders from scratch. Delivered right to your door or ready for carrying out - ordering restaurant food has never been cheaper, faster, easier or more fun!

We are releasing many more features in the next version which would enable you to interact with a lot of good features, so stay tuned :slightly_smiling_face:

You can say:

"Alexa, open kbob's and place an order"
"Alexa, start kbob's"
"Alexa, launch kbob's"

Please leave a 5-star review if you love this skill, or send us an email with your suggestions at 'hello@datavizz.in'. Your 5-star reviews encourage us to keep making more great skills, thanks!
